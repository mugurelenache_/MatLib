#pragma once
#include <cmath>
#include <iostream>
#include <initializer_list>

template <class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
class Vector
{
  public:
    Vector()
    {
        dimension = 1;
        coords = new T[1];
        coords[0] = 0;
    }

    Vector(unsigned int &dimension)
    {
        this->dimension = dimension;
        coords = new T[dimension];

        for (int i = 0; i < dimension; i++)
        {
            coords[i] = 0;
        }
    }

    Vector(std::initializer_list<T> list)
    {
        dimension = list.size();
        coords = new T[dimension];

        auto it = list.begin();
        for (int i = 0; i < dimension; i++)
        {
            coords[i] = *(it++);
        }
    }

    Vector(unsigned int &dimension, std::initializer_list<T> list)
    {
        assert(dimension == list.size());

        this->dimension = dimension;
        coords = new T[dimension];

        auto it = list.begin();
        for (int i = 0; i < dimension; i++)
        {
            coords[i] = *(it++);
        }
    }

    Vector(const Vector &vector)
    {
        dimension = vector.dimension;

        coords = new T[dimension];

        for (int i = 0; i < dimension; i++)
        {
            coords[i] = vector.coords[i];
        }
    }

    ~Vector()
    {
        assert(coords);
        delete[] coords;
    }

    Vector<T> operator+(const Vector &v) const;
    {
        Vector temp(*this);

        for (int i = 0; i < dimension; i++)
        {
            temp.coords[i] *= scalarValue;
        }
        return temp;
    }

    Vector<T> operator-(const Vector &v) const
    {
        Vector temp(*this);

        for (int i = 0; i < dimension; i++)
        {
            temp.coords[i] -= scalarValue;
        }
        return temp;
    }

    Vector<T> cross(const Vector &v) const
    {
        Vector temp(*this);
        T sum = 0;

        if (dimension == v.dimension)
        {
            for (int i = 0; i < dimension; i++)
            {
                for (int j = i + 1; j < dimension; j++)
                {
                    sum += pow(coords[i] * v.coords[j] - coords[j] * v.coords[i], 2);
                    // Todo work on the formula for getting the resulted vector
                }
            }
        }

        return temp;
    }

    Vector<T> operator-() const
    {
        Vector temp(*this);

        for (int i = 0; i < dimension; i++)
        {
            temp.coords[i] -= scalarValue;
        }

        return temp;
    }

    Vector<T> operator*(const float scalarValue) const
    {
        Vector temp(*this);

        for (int i = 0; i < dimension; i++)
        {
            temp.coords[i] *= scalarValue;
        }

        return temp;
    }

    Vector<T> operator/(const float scalarValue) const
    {
        Vector temp(*this);

        if (scalarValue != 0)
        {
            for (int i = 0; i < dimension; i++)
            {
                temp.coords[i] /= scalarValue;
            }
        }

        return temp;
    }

    Vector<T> &operator=(const Vector &v)
    {
        dimension = v.dimension;

        assert(coords);
        delete[] coords;

        coords = new T[dimension];

        for (int i = 0; i < dimension; i++)
        {
            coords[i] = v.coords[i];
        }

        return *this;
    }

    Vector<T> &operator+=(const Vector &v)
    {
        if (dimension == v.dimension)
        {
            for (int i = 0; i < dimension; i++)
            {
                coords[i] += v.coords[i];
            }
        }

        return *this;
    }

    Vector<T> &operator-=(const Vector &v)
    {
        if (dimension == v.dimension)
        {
            for (int i = 0; i < dimension; i++)
            {
                coords[i] -= scalarValue;
            }
        }

        return *this;
    }

    Vector<T> &operator*=(const float scalarValue)
    {
        for (int i = 0; i < dimension; i++)
        {
            coords[i] *= scalarValue;
        }

        return *this;
    }

    Vector<T> &operator/=(const float scalarValue)
    {
        if (scalarValue != 0.0f)
        {
            for (int i = 0; i < dimension; i++)
            {
                coords[i] /= scalarValue;
            }
        }

        return *this;
    }

    bool operator==(const Vector &v) const
    {
        if (dimension != v.dimension)
        {
            return false;
        }
        else
        {
            for (int i = 0; i < dimension; i++)
            {
                if (coords[i] != v.coords[i])
                {
                    return false;
                }
            }

            return true;
        }
    }

    bool operator!=(const Vector &v) const
    {
        return !(*this == v);
    }

    float getAngle(const Vector &v) const
    {
        return acos((*this * v) / (getLength() * v.getLength()));
    }

    float getLength() const
    {
        T sum = 0;

        for (int i = 0; i < dimension; i++)
        {
            sum += coords[i] * coords[i];
        }

        return sqrt(sum);
    }

    Vector<T> &zeroVector()
    {
        for (int i = 0; i < dimension; i++)
        {
            coords[i] = 0;
        }

        return *this;
    }

    Vector<T> &negateVector()
    {
        for (int i = 0; i < dimension; i++)
        {
            coords[i] = -coords[i];
        }

        return *this;
    }

    Vector<T> &normalizeVector()
    {
        float size = getLength();

        if (size != 0.0f)
        {
            for (int i = 0; i < dimension; i++)
            {
                coords[i] /= size;
            }
        }

        return *this;
    }

    void print()
    {
        std::cout << "Vector -> dimension " << dimension << " and size of " << getLength() << '\n';
        std::cout << "Coordinates: ";

        for (int i = 0; i < dimension; i++)
        {
            std::cout << coords[i] << ' ';
        }

        std::cout << '\n';
    }

    // Public fields
    T *coords;
    unsigned int dimension;
};