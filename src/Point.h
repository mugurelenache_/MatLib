#pragma once
#include <cmath>
#include <assert.h>
#include <initializer_list>

template <class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
class Point
{
  public:
    Point()
    {
        dimension = 1;
        coords = new T[1];
        coords[0] = 0;
    }

    Point(unsigned int &dimension)
    {
        this->dimension = dimension;
        coords = new T[dimension];

        for (int i = 0; i < dimension; i++)
            coords[i] = 0;
    }

    Point(std::initializer_list<T> list)
    {
        dimension = list.size();
        coords = new T[dimension];

        auto it = list.begin();

        for (int i = 0; i < dimension; i++)
            coords[i] = *(it++);
    }

    Point(unsigned int &dimension, std::initializer_list<T> list)
    {
        assert(dimension == list.size());

        this->dimension = dimension;
        coords = new T[dimension];

        auto it = list.begin();

        for (int i = 0; i < dimension; i++)
            coords[i] = *(it++);
    }

    Point(const Point &point)
    {
        dimension = point.dimension;

        coords = new T[dimension];

        for (int i = 0; i < dimension; i++)
            coords[i] = point.coords[i];
    }

    ~Point()
    {
        assert(coords);
        delete coords;
    }

    bool operator==(const Point &point) const
    {
        if (dimension == point.dimension)
        {
            for (int i = 0; i < dimension; i++)
                if (coords[i] != point.coords[i])
                    return false;
            return true;
        }
        return false;
    }

    bool operator!=(const Point &point) const
    {
        return !(*this == point);
    }

    Point<T> &operator=(const Point &point)
    {
        this->dimension = point.dimension;

        assert(coords);
        delete[] coords;

        coords = new T[point.dimension];

        for (int i = 0; i < dimension; i++)
            coords[i] = point.coords[i];

        return *this;
    }

    // Euclidean Distance between 2 points
    float eDistance(const Point &point) const
    {
        float sum = 0;

        if (dimension == point.dimension)
        {
            for (int i = 0; i < this->dimension; i++)
            {
                sum += pow((coords[i] - point.coords[i]), 2);
            }
        }
        else
        {
            // Reduce dimension to the minium one
            if (this->dimension < point.dimension)
            {
                for (int i = 0; i < this->dimension; i++)
                {
                    sum += pow((coords[i] - point.coords[i]), 2);
                }
            }
            else
            {
                for (int i = 0; i < point.dimension; i++)
                {
                    sum += pow((coords[i] - point.coords[i]), 2);
                }
            }
        }

        return sqrt(sum);
    }

    // Manhattan Distance between 2 points
    float mDistance(const Point &point) const
    {
        float sum = 0;

        if (this->dimension == point.dimension)
        {
            for (int i = 0; i < this->dimension; i++)
            {
                sum += fabs(coords[i] - point.coords[i]);
            }
        }
        else
        {
            // Reduce dimension to the minium one
            if (this->dimension < point.dimension)
            {
                for (int i = 0; i < this->dimension; i++)
                {
                    sum += fabs(coords[i] - point.coords[i]);
                }
            }
            else
            {
                for (int i = 0; i < point.dimension; i++)
                {
                    sum += fabs(coords[i] - point.coords[i]);
                }
            }
        }

        return sum;
    }

    // Public fields
    T *coords;
    unsigned int dimension;
};