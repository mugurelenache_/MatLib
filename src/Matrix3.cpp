#include "Matrix3.h"
#include <cassert>
#include <iostream>

/**  ========================== Matrix Constructors ========================== */

Matrix3::Matrix3()
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = 0;
        }
    }
}

Matrix3::Matrix3(bool doNothing)
{
    // This basically does nothing, not going through the initialization of the matrix
    // in order to achieve better optimisation - usually use the doNothign boolean as false (convention?)
}

Matrix3::Matrix3(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");

    auto it = coefficients.begin();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = *(it++);
        }
    }
}
    
Matrix3::Matrix3(const float matrix[3][3])
{
    for(char i = 0; i < 3; i++)
    {
        for (char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = matrix[i][j];
        }
    }
}
    
Matrix3::Matrix3(const Matrix3& matrixReference)
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = matrixReference.matrix[i][j];
        }
    }
}

/**  ========================== Matrix Methods (OTHERS) ========================== */

void Matrix3::printMatrix()
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            std::cout << this->matrix[i][j] << ' ';
        }

        std::cout << '\n';
    }
}

float Matrix3::determinant()
{
    return    matrix[0][0] * matrix[1][1] * matrix[2][2]
            + matrix[1][0] * matrix[2][1] * matrix[0][2]
            + matrix[0][1] * matrix[1][2] * matrix[2][0] 
            - matrix[0][2] * matrix[1][1] * matrix[2][0] 
            - matrix[2][1] * matrix[1][2] * matrix[0][0] 
            - matrix[0][1] * matrix[1][0] * matrix[2][2];
}

/**  ========================== Matrix Addition ========================== */

Matrix3 Matrix3::operator+(const Matrix3& matrixReference)
{
    Matrix3 tempMatrix = Matrix3(false);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] + matrixReference.matrix[i][j];
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator+(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");

    Matrix3 tempMatrix = Matrix3(false);

    auto it = coefficients.begin();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] + *(it++);
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator+(const float matrix[3][3])
{
    Matrix3 tempMatrix = Matrix3(false);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] + matrix[i][j];
        }
    }

    return tempMatrix;
}


/**  ========================== Matrix Addition EQ ========================== */

Matrix3& Matrix3::operator+=(const Matrix3& matrixReference)
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] += matrixReference.matrix[i][j];
        }
    }

    return *this;
}
    
Matrix3& Matrix3::operator+=(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");

    auto it = coefficients.begin();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] += *(it++);
        }
    }

    return *this;
}

Matrix3& Matrix3::operator+=(const float matrix[3][3])
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] += matrix[i][j];
        }
    }

    return *this;
}

/**  ========================== Matrix Subtraction ========================== */

Matrix3 Matrix3::operator-(const Matrix3& matrixReference)
{
    Matrix3 tempMatrix = Matrix3(false);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] - matrixReference.matrix[i][j];
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator-(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");
    
    auto it = coefficients.begin();

    Matrix3 tempMatrix = Matrix3(false);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] - *(it++);
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator-(const float matrix[3][3])
{
    Matrix3 tempMatrix = Matrix3(false);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            tempMatrix.matrix[i][j] = this->matrix[i][j] - matrix[i][j];
        }
    }

    return tempMatrix;
}


/**  ========================== Matrix Operator EQ ========================== */

Matrix3& Matrix3::operator-=(const Matrix3& matrixReference)
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] -= matrixReference.matrix[i][j];
        }
    }

    return *this;
}

Matrix3& Matrix3::operator-=(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");

    auto it = coefficients.begin();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] -= *(it++);
        }
    }

    return *this;
}

Matrix3& Matrix3::operator-=(const float matrix[3][3])
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] -= matrix[i][j];
        }
    }

    return *this;
}

/**  ========================== Matrix Multiplication ========================== */

Matrix3 Matrix3::operator*(const Matrix3& matrixReference)
{
    Matrix3 tempMatrix = Matrix3();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] + matrixReference.matrix[k][j];
            }
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator*(const std::initializer_list<float>& coefficients)
{
    Matrix3 tempMatrix = Matrix3(coefficients);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] + tempMatrix.matrix[k][j];
            }
        }
    }

    return tempMatrix;
}

Matrix3 Matrix3::operator*(const float matrix[3][3])
{
    Matrix3 tempMatrix = Matrix3();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] + matrix[k][j];
            }
        }
    }

    return tempMatrix;
}

/**  ========================== Matrix Multiplication EQ ========================== */

Matrix3& Matrix3::operator*=(const Matrix3& matrixReference)
{
    Matrix3 tempMatrix = Matrix3();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] * matrixReference.matrix[k][j];
            }
        }
    }

    *this = tempMatrix;

    return *this;
}

Matrix3& Matrix3::operator*=(const std::initializer_list<float>& coefficients)
{
    Matrix3 tempMatrix = Matrix3();
    Matrix3 coefMatrix = Matrix3(coefficients);

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] * coefMatrix.matrix[k][j];
            }
        }
    }

    *this = tempMatrix;

    return *this;
}

Matrix3& Matrix3::operator*=(const float matrix[3][3])
{
    Matrix3 tempMatrix = Matrix3();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            for(char k = 0; k < 3; k++)
            {
                tempMatrix.matrix[i][j] += this->matrix[i][k] * matrix[k][j];
            }
        }
    }

    *this = tempMatrix;

    return* this;
}


/**  ========================== Matrix Comparison ========================== */

bool Matrix3::operator==(const Matrix3& matrixReference)
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            if(this->matrix[i][j] != matrixReference.matrix[i][j])
            {
                return false;
            }
        }
    }

    return true;
}

/**  ========================== Matrix EQ ========================== */

Matrix3& Matrix3::operator=(const Matrix3& matrixReference)
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = matrixReference.matrix[i][j];
        }
    }

    return *this;
}

Matrix3& Matrix3::operator=(const std::initializer_list<float>& coefficients)
{
    assert((coefficients.size() == 9) && "The number of coefficients is invalid. It must be 9 (3x3 Matrix).");

    auto it = coefficients.begin();

    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = *(it++);
        }
    }

    return *this;
}

Matrix3& Matrix3::operator=(const float matrix[3][3])
{
    for(char i = 0; i < 3; i++)
    {
        for(char j = 0; j < 3; j++)
        {
            this->matrix[i][j] = matrix[i][j];
        }
    }

    return *this;
}

