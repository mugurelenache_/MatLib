#pragma once
#include <cmath>
#include <assert.h>
#include <initializer_list>
#include <iostream>

#define endl '\n'

template <class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
class Matrix
{
  public:
    Matrix(const int &n)
    {
        numberR = n;
        numberC = n;

        mat = new T *[numberR];

        for (int i = 0; i < numberR; i++)
        {
            mat[i] = new T[numberC];
            for (int j = 0; j < numberC; j++)
            {
                mat[i][j] = 0;
            }
        }
    }

    Matrix(const int &row, const int &col)
    {
        numberR = row;
        numberC = col;

        mat = new T *[numberR];

        for (int i = 0; i < numberR; i++)
        {
            mat[i] = new T[numberC];
            for (int j = 0; j < numberC; j++)
            {
                mat[i][j] = 0;
            }
        }
    }

    Matrix(const int &row, const int &col, const std::initializer_list<T> &list)
    {
        if (list.size() == row * col)
        {
            numberR = row;
            numberC = col;

            mat = new T *[numberR];
            auto k = list.begin();

            for (int i = 0; i < numberR; i++)
            {
                mat[i] = new T[numberC];

                for (int j = 0; j < numberC; j++)
                {
                    mat[i][j] = *(k++);
                }
            }
        }
    }

    Matrix(const Matrix &matrix)
    {
        numberC = matrix.numberC;
        numberR = matrix.numberR;

        mat = new T *[numberR];

        for (int i = 0; i < numberR; i++)
        {
            mat[i] = new T[numberC];

            for (int j = 0; j < numberC; j++)
            {
                mat[i][j] = matrix.mat[i][j];
            }
        }
    }

    Matrix(const std::initializer_list<T> &list)
    {
        if (list.size() == sqrt(list.size()) * sqrt(list.size()))
        {
            auto k = list.begin();

            numberC = numberR = sqrt(list.size());

            mat = new T *[numberR];

            for (int i = 0; i < numberR; i++)
            {
                mat[i] = new T[numberC];

                for (int j = 0; j < numberC; j++)
                {
                    mat[i][j] = *(k++);
                }
            }
        }
    }

    ~Matrix()
    {
        for (int i = 0; i < numberR; i++)
        {
            delete[] mat[i];
        }
        delete[] mat;
        //std::cout << "Matrix of: " << this->numberR << 'x' << this->numberC << " at > " << this << "< has been successfully destroyed" << endl;
    }

    T get(const int &row, const int &col) const
    {
        assert((row < numberR && col < numberC));
        return mat[row][col];
    }

    Matrix<T> transpose()
    {
        Matrix temp(*this);

        for (int i = 0; i < numberR; i++)
        {
            for (int j = 0; j < numberC; j++)
            {
                temp.mat[i][j] = mat[j][i];
            }
        }

        return temp;
    }

    Matrix<T> &modifyRow(const int &rowNo, const std::initializer_list<T> &&list)
    {
        if (rowNo < numberR && list.size() == numberC)
        {
            auto k = list.begin();

            for (int i = 0; i < numberC; i++)
            {
                mat[rowNo][i] = *(k++);
            }
        }

        return (*this);
    }

    Matrix<T> &modifyColumn(int columnNo, const std::initializer_list<T> &list)
    {
        if (columnNo < numberC && list.size() == numberR)
        {
            auto k = list.begin();

            for (int i = 0; i < numberR; i++)
            {
                mat[i][columnNo] = *(k++);
            }
        }

        return (*this);
    }

    void print()
    {
        std::cout << "Matrix size of: " << numberR << 'x' << numberC << endl;

        for (int i = 0; i < numberR; i++, std::cout << endl)
        {
            for (int j = 0; j < numberC; j++, std::cout << ' ')
            {
                std::cout << mat[i][j];
            }
        }
    }

    Matrix<T> operator+(const Matrix &matrix)
    {
        if (numberC == matrix.numberC && numberR == matrix.numberR)
        {
            Matrix<T> temp(numberC, numberR);

            for (int i = 0; i < temp.numberR; i++)
            {
                for (int j = 0; j < temp.numberC; j++)
                {
                    temp.mat[i][j] = mat[i][j] + matrix.mat[i][j];
                }
            }

            return temp;
        }

        std::cout << "The operation cannot be made, please check your matrices" << endl;
        return *this;
    }

    Matrix<T> operator-(const Matrix &matrix)
    {
        if (numberC == matrix.numberC && numberR == matrix.numberR)
        {
            Matrix<T> temp(numberC, numberR);

            for (int i = 0; i < temp.numberR; i++)
            {
                for (int j = 0; j < temp.numberC; j++)
                {
                    temp.mat[i][j] = mat[i][j] - matrix.mat[i][j];
                }
            }

            return temp;
        }

        std::cout << "The operation cannot be made, please check your matrices" << endl;
        return *this;
    }

    Matrix<T> operator*(const Matrix &matrix)
    {
        if (numberC == matrix.numberR)
        {
            Matrix<T> temp(matrix.numberR, this->numberC);

            for (int i = 0; i < temp.numberR; i++)
            {
                for (int j = 0; j < temp.numberC; j++)
                {
                    for (int k = 0; k < temp.numberR; k++)
                    {
                        temp.mat[i][j] += mat[i][k] * matrix.mat[k][j];
                    }
                }
            }

            return temp;
        }

        std::cout << "The operation cannot be made, please check your matrices" << endl;
        return *this;
    }

    Matrix<T> operator*(const T &n)
    {
        Matrix<T> temp(numberR, numberC);

        for (int i = 0; i < temp.numberR; i++)
        {
            for (int j = 0; j < temp.numberC; j++)
            {
                temp.mat[i][j] = mat[i][j] * n;
            }
        }

        return temp;
    }

    Matrix<T> &operator=(const Matrix &matrix)
    {
        for (int i = 0; i < numberR; i++)
            delete[] mat[i];
        delete[] mat;

        numberR = matrix.numberR;
        numberC = matrix.numberC;

        mat = new T *[numberR];

        for (int i = 0; i < numberR; i++)
        {
            mat[i] = new T[numberC];
            for (int j = 0; j < numberC; j++)
            {
                mat[i][j] = matrix.mat[i][j];
            }
        }

        return *this;
    }

    Matrix<T> &operator+=(const Matrix &matrix)
    {
        *this = *this + matrix;
        return *this;
    }

    Matrix<T> &operator-=(const Matrix &matrix)
    {
        *this = *this - matrix;
        return *this;
    }

    Matrix<T> &operator*=(const Matrix &matrix)
    {
        *this = *this * matrix;
        return *this;
    }

    Matrix<T> &operator*=(const T &n)
    {
        *this = *this * n;
        return *this;
    }

    bool operator==(const Matrix &matrix) const
    {
        if (numberC == matrix.numberC && numberR == matrix.numberR)
        {
            for (int i = 0; i < numberR; i++)
            {
                for (int j = 0; j < numberC; j++)
                {
                    if (mat[i][j] != matrix.mat[i][j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    bool operator!=(const Matrix &matrix) const
    {
        if (numberC == matrix.numberC && numberR == matrix.numberR)
        {
            return !(*this == matrix);
        }
        return false;
    }

    Matrix<T> createSubmatrix(const int &rowNo, const int &columnNo)
    {
        if (this->numberR > 1)
        {
            int tempI = 0;
            int tempJ = 0;

            Matrix<T> temp(numberR - 1, numberC - 1);

            for (int i = 0; i < numberR; i++, tempI++, tempJ = 0)
                for (int j = 0; j < numberC; j++)
                {
                    if (i == rowNo)
                    {
                        i++;
                        j = 0;
                        tempJ = 0;

                        if (i >= numberR)
                            break;

                        continue;
                    }

                    if (j == columnNo)
                    {
                        continue;
                    }

                    temp.mat[tempI][tempJ] = this->mat[i][j];
                    tempJ++;
                }

            return temp;
        }

        return Matrix(1, 1, {this->mat[rowNo][columnNo]});
    }

    T determinant()
    {
        T value = 0;

        if (numberR == numberC)
        {
            switch (numberR)
            {
            case 1:
                value = this->mat[0][0];
                break;

            case 2:
                value = this->mat[0][0] * this->mat[1][1] - this->mat[0][1] * this->mat[1][0];
                break;

            default:
                for (int i = 0; i < numberR; i++)
                {
                    Matrix temp = this->createSubmatrix(i, 0);
                    value += pow(-1, i) * temp.determinant() * this->mat[i][0];
                }
                break;
            }
        }

        return value;
    }

    // Due to type conversion it may return slightly inaccurate inverses such that the result of A * (A^(-1)) will not always be equal to Identity Matrix
    Matrix<T> inverse()
    {
        if (this->determinant() != 0)
        {
            Matrix<T> transpose = this->transpose();
            Matrix<T> inverted(numberR, numberC);

            T det = this->determinant();

            for (int i = 0; i < numberR; i++)
            {
                for (int j = 0; j < numberC; j++)
                {
                    inverted.mat[i][j] = transpose.createSubmatrix(i, j).determinant() * pow(-1, i + j) / det;
                }
            }

            return inverted;
        }

        std::cout << "\nThis matrix does not have a determinant.\n";
        return (*this);
    }

    static Matrix<T> identity(const int &n)
    {
        Matrix<T> temp(n);

        for (int i = 1; i < n; i++)
        {
            temp.mat[i][i] = 1;
        }

        return temp;
    }

    // Public fields
    T **mat;
    int numberC, numberR;
};