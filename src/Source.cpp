#include "Matrix3.h"
#include <iostream>

int main() 
{
    Matrix3 m = Matrix3({32, 3, 4, 5, 5, 2, 4, 6, 3});
    Matrix3 m2 = Matrix3({32, 3, 4, 5, 5, 2, 4, 6, 3});

	m += m2;

	for(int i = 0; i < 100000000; i++)
	{
		m += m2;
	}

	m.printMatrix();
	std::cout << m.determinant();
    return 0;
}
