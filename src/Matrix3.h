#pragma once
#include <initializer_list>

/*
 * More specialised type of matrix (3x3)
 */

class Matrix3
{
    public: // Fields
    float matrix[3][3];

    public: // Methods
    
    // Default Constructor
    Matrix3();

    // Special Do-Nothing Constructor
    Matrix3(bool doNothing);

    // Constructor that takes a Matrix3 reference
    Matrix3(const Matrix3& matrixReference);
    
    // Constructor that takes a coefficient list as a parameter 
    Matrix3(const std::initializer_list<float>& coefficients);

    // Constructor that takes a matrix of floats as a parameter
    Matrix3(const float matrix[3][3]);

    // Debug method that prints out the matrix
    void printMatrix();

    // Matrix determinant
    float determinant();

    // >> TODO Complete functions
    Matrix3 transpose();
    Matrix3 inverse();

    /** ===========================  Overloaded Operators =========================== */

    // Matrix addition
    Matrix3 operator+(const Matrix3& matrixReference);
    Matrix3 operator+(const std::initializer_list<float>& coefficients);
    Matrix3 operator+(const float matrix[3][3]);

    // Matrix addition then equal
    Matrix3& operator+=(const Matrix3& matrixReference);
    Matrix3& operator+=(const std::initializer_list<float>& coefficients);
    Matrix3& operator+=(const float matrix[3][3]);
    
    // Matrix subtraction
    Matrix3 operator-(const Matrix3& matrixReference);
    Matrix3 operator-(const std::initializer_list<float>& coefficients);
    Matrix3 operator-(const float matrix[3][3]);

    // Matrix subtraction then equal
    Matrix3& operator-=(const Matrix3& matrixReference);
    Matrix3& operator-=(const std::initializer_list<float>& coefficients);
    Matrix3& operator-=(const float matrix[3][3]);

    // Matrix multiplication
    Matrix3 operator*(const Matrix3& matrixReference);
    Matrix3 operator*(const std::initializer_list<float>& coefficients);
    Matrix3 operator*(const float matrix[3][3]);

    // Matrix multiplication then equal
    Matrix3& operator*=(const Matrix3& matrixReference);
    Matrix3& operator*=(const std::initializer_list<float>& coefficients);
    Matrix3& operator*=(const float matrix[3][3]);

    // Matrix assignment
    Matrix3& operator=(const Matrix3& matrixReference);
    Matrix3& operator=(const std::initializer_list<float>& coefficients);
    Matrix3& operator=(const float matrix[3][3]);


    // Matrix comparison
    bool operator==(const Matrix3& matrixReference);
    
    
    /** =========================== End Overload Operators =========================== */
};